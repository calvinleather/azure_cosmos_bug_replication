"""
Contains shared functionality used to query data from mongo/cosmos.
"""
import os
import pymongo
import logging
import sys


# Get client
from __app__.shared_functions.constants import CUSTOMER_NUMBER, RULES_COLLECTION, CUSTOMERS_COLLECTION, \
    RULE_ANTECEDENT, UPC, RANKED_UPC_RECORDS_COLLECTION, LEVEL_4_CATEGORY, N_ITEMS_TO_RETURN, LATENT_VECTOR


def get_client():
    """Returns a mongo client object to be used to access the database"""
    # TODO: Try to get read only MONGO_URI for safety
    # TODO: Is there a more performant way to do this than pymongo?
    logging.info("Getting database client for database :"
                 f"{os.environ['DATABASE_NAME']}. MONGO_URI found: {'MONGO_URI' in os.environ}")
    client = pymongo.MongoClient(os.environ["MONGO_URI"])
    return client[os.environ["DATABASE_NAME"]]


# Wraps mongo query used to grab relevant rules from database
def relevant_rules_query(upc, cart):
    """Structures mongo query object to find rules matching the current item + cart"""
    return [{
        "$project":
            {
                RULE_ANTECEDENT: 1,
                "consequent": 1,
                "features": 1,
                "subset": {
                    "$setIsSubset": [
                        f"${RULE_ANTECEDENT}",
                        [upc] + cart
                    ]
                }
            }
    },
        {
            "$match": {
                "subset": True
            }
        }
    ]


# Wraps mongo query used to grab a customer object from the database
def customer_vector_query(customer_id: str):
    """Structures a find query to find the customer by the customer number"""
    return {CUSTOMER_NUMBER: str(customer_id)}


# Wraps mongo query used to grab a item object from the database
def find_item_by_upc(upc: int):
    """Structures a find query to pull the requested item from the backfill collection"""
    return {UPC: int(upc)}


def find_top_5_items_query(level_4_category: str):
    """Returns items sorted by popularity for a given category from the backfill collection"""
    return {LEVEL_4_CATEGORY: str(level_4_category)}


def get_relevant_rules(db, current_item_upc: int, basket_upcs: [int]):
    """Executes the mongo command from {{relevant_rules_query}} and performs light formatting of the results"""
    # TODO: make this async
    # relevant_rules = db[RULES_COLLECTION].aggregate(
    #     relevant_rules_query(current_item_upc, basket_upcs)
    # )
    # TODO: figure out why $setissubset isn't working
    relevant_rules = db[RULES_COLLECTION].find({RULE_ANTECEDENT: int(current_item_upc)})
    # Check if relevant_rules were returned
    relevant_rules = list(relevant_rules)
    if len(relevant_rules) == 0:
        return None
    return list(relevant_rules)


def get_customer_vector(db, customer_id: int) -> [int]:
    """Executes the mongo command from {{customer_vector_query}} and performs light formatting of the results"""
    # TODO: make this async
    customer = db[CUSTOMERS_COLLECTION].find_one(
        customer_vector_query(customer_id)
    )
    # Check if a customer was returned
    if customer is None:
        return None
    return customer[LATENT_VECTOR]


def get_top_items_backfill(db, upc: int) -> [int]:
    """Executes several queries to obtain the backfill for the current item"""
    # TODO: make this async
    # TODO: add in ability to blacklist UPCs (e.g., UPCs that are already in the return set because of rules)
    item = db[RANKED_UPC_RECORDS_COLLECTION].find_one(
        find_item_by_upc(upc)
    )
    # Check if an item was returned
    if item is None:
        return None

    # TODO: Figure out issue with calling .sort("rank") (getting wierd azure errors, plus some items had same rank)
    top_items_in_category = db[RANKED_UPC_RECORDS_COLLECTION].find(
        find_top_5_items_query(item[LEVEL_4_CATEGORY])
    ).limit(2*N_ITEMS_TO_RETURN)
    top_items_in_category = list(top_items_in_category)
    if len(top_items_in_category) < N_ITEMS_TO_RETURN:
        logging.info(f"Found category with less than {N_ITEMS_TO_RETURN} : {item[LEVEL_4_CATEGORY]}")
    return [x[UPC] for x in top_items_in_category]
