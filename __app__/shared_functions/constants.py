"""
Contains variables that are shared across the predictions function.
In particular, contains names of collection and documents to simplify
editing of mongo schema.
"""

# Collection names
RULES_COLLECTION = "rules"
CUSTOMERS_COLLECTION = "customer"
RANKED_UPC_RECORDS_COLLECTION = "category"

# Document keys
CUSTOMER_NUMBER = "cust_nbr"
LATENT_VECTOR = "features"
RULE_ANTECEDENT = "antecedent"
RULE_CONSEQUENT = "consequent"
UPC = "upc"
LEVEL_4_CATEGORY = "prdt_lvl_4_dsc"  # The category used in the windowing function to rank items for backfill.

#Numerical settings
N_ITEMS_TO_RETURN = 8  # The number of recommendations to be returned
