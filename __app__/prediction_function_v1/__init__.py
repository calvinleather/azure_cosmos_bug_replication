"""
__init__.py
====================================
Contains the main prediction function logic to be called by Azure Functions.
"""

from azure.functions import HttpRequest, HttpResponse
import logging
import sys

from __app__.shared_functions.constants import N_ITEMS_TO_RETURN, UPC, RULE_CONSEQUENT, CUSTOMERS_COLLECTION
from ..shared_functions.database_queries import get_client,

CLIENT = None


def main(req: HttpRequest) -> HttpResponse:
    logging.info(f"Received request: {req.params}")
    # Ensure connection to database
    global CLIENT
    if CLIENT is None:
        CLIENT = get_client()

    return "test"


if __name__ == "__main__":
    req = HttpRequest("GET", "test.com", body=b"")
    print(main(req))
